import 'package:daily_meal/screen/category/category_screen.dart';
import 'package:daily_meal/screen/favorite_screen.dart';
import 'package:flutter/material.dart';

class TopTabScreen extends StatefulWidget {
  @override
  _TopTabScreenState createState() => _TopTabScreenState();
}

class _TopTabScreenState extends State<TopTabScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Meal'),
          bottom: TabBar(tabs: [
            Tab(icon: Icon(Icons.category), text: 'Category'),
            Tab(icon: Icon(Icons.star), text: 'Favorite')
          ]),
        ),
        body: TabBarView(children: [
          CategoryScreen(),
          FavoriteScreen(),
        ]),
      ),
    );
  }
}
