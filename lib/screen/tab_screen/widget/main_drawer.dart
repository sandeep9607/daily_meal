import 'package:daily_meal/screen/favorite_screen.dart';
import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.center,
            color: Theme.of(context).primaryColor.withOpacity(0.9),
            child: Text('Meal App'),
          ),
          ListTile(
            leading: Icon(Icons.restaurant),
            title: Text('Home'),
            onTap: () {
              Navigator.pushNamed(context, '/');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.star,
            ),
            title: Text('Favorite'),
            onTap: () {
              Navigator.pushNamed(context, FavoriteScreen.routeName);
            },
          ),
        ],
      ),
    );
  }
}
