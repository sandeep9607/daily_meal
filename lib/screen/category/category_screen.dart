import 'package:daily_meal/data/dummy_data.dart';
import 'package:daily_meal/screen/category/widget/category_item.dart';
import 'package:flutter/material.dart';
// import '../../model/category.dart';

class CategoryScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.add),
      ),
      body: GridView(
          padding: EdgeInsets.all(16),
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20,
          ),
          children: DUMMY_CATEGORIES.map((catData) {
            return CategoryItem(
              id: catData.id,
              title: catData.title,
              color: catData.color,
            );
          }).toList()),
    );
  }
}
