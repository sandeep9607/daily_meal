import 'package:flutter/material.dart';

class Category {
  final String id;
  final String title;
  final Color color;

  const Category({
    @required this.id,
    @required this.title,
    this.color = Colors.orange,
  });
}

const DUMMY_CATEGORIES = const [
  Category(id: 'id1', title: 'Italian', color: Colors.yellow),
  Category(id: 'id2', title: 'Chinese', color: Colors.green),
  Category(id: 'id3', title: 'Indian', color: Colors.blue),
  Category(id: 'id4', title: 'South Indian', color: Colors.grey),
  Category(id: 'id5', title: 'North Indian', color: Colors.red),
];
