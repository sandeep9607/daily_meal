import 'package:daily_meal/screen/favorite_screen.dart';
import 'package:daily_meal/screen/meal_detail_screen/meal_detail_screen.dart';
import 'package:daily_meal/screen/meal_screen/category_meal_screen.dart';
import 'package:daily_meal/screen/tab_screen/bottom_tab_screen.dart';
import 'package:daily_meal/screen/tab_screen/top_tab_screen.dart';
import 'package:daily_meal/util/color.dart';
import 'package:flutter/material.dart';

import 'screen/category/category_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: appPrimary,
        accentColor: Color(0xFFef0078),
        // canvasColor: Colors.green,
        fontFamily: 'Poppins',
        textTheme: ThemeData.light().textTheme.copyWith(
              bodyText1: TextStyle(color: Color.fromRGBO(20, 51, 51, 1)),
              headline6: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
            ),
        appBarTheme: AppBarTheme(
            textTheme: ThemeData.light().textTheme.copyWith(
                    headline6: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                  fontSize: 20,
                )),
            iconTheme: ThemeData.light().iconTheme.copyWith(
                  color: Colors.white,
                )),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      // home: CategoryScreen(),
      initialRoute: '/',
      routes: {
        '/': (ctx) => BottomTabScreen(),
        CategoryMealScreen.routeName: (ctx) => CategoryMealScreen(),
        MealDetailScreen.routeName: (ctx) => MealDetailScreen(),
        FavoriteScreen.routeName: (ctx) => FavoriteScreen(),
      },
    );
  }
}
