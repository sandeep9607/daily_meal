import 'package:flutter/material.dart';

MaterialColor appPrimary = const MaterialColor(0xfffa8100, const {
  50: const Color(0xfffff2df),
  100: const Color(0xffffddb0),
  200: const Color(0xffffc77d),
  300: const Color(0xffffaf49),
  400: const Color(0xffff9e22),
  500: const Color(0xffff8d00),
  600: const Color(0xfffa8100),
  700: const Color(0xfff47100),
  800: const Color(0xffee6002),
  900: const Color(0xffe54304)
});
